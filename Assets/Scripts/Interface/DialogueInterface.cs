﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueInterface : MonoBehaviour
{

    public Image dialogueBox;
    public Text dialogueTextObject, nameTextObject;
    public Button contButton;
    public Button choiceButton;

    string dialogueText;
    private string[] _choices;

    private bool _showing = false;
    private bool _windowShowing = false;
    private bool _selectionClicked = false;

    private CameraController cam;

    void Awake()
    {
        Dialoguer.Initialize();
        cam = Camera.main.GetComponent<CameraController>();
    }

    void Start()
    {
        addDialoguerEvents();
    }

    void Update()
    {
        if (!_showing) return;
        if (!_windowShowing) return;

        dialogueTextObject.text = dialogueText;
        nameTextObject.text = Dialoguer.GetGlobalString(1);
        //contButton.Select();
    }

    public void addDialoguerEvents()
    {
        Dialoguer.events.onStarted += onStartedHandler;
        Dialoguer.events.onEnded += onEndedHandler;
        Dialoguer.events.onInstantlyEnded += onInstantlyEndedHandler;
        Dialoguer.events.onTextPhase += onTextPhaseHandler;
        Dialoguer.events.onWindowClose += onWindowCloseHandler;
    }

    private void onStartedHandler()
    {
        _showing = true;
        iTween.ScaleTo(dialogueBox.gameObject, iTween.Hash("scale", Vector3.one, "time", .5f));
        Dialoguer.SetGlobalBoolean(0, true);
    }

    private void onEndedHandler()
    {
        _showing = false;
    }

    private void onInstantlyEndedHandler()
    {
        _showing = true;
        _windowShowing = false;
        _selectionClicked = false;
    }

    private void onTextPhaseHandler(DialoguerTextData data)
    {
        dialogueText = data.text;

        if (data.windowType == DialoguerTextPhaseType.Text)
        {
            _choices = null;
            
        }
        else
        {
            _choices = data.choices;
        }

        _windowShowing = true;
        _selectionClicked = false;
    }

    private void onWindowCloseHandler()
    {
        _windowShowing = false;
        _selectionClicked = false;

        iTween.ScaleTo(dialogueBox.gameObject, iTween.Hash("scale", Vector3.zero, "time", .5f));
        Dialoguer.SetGlobalBoolean(0, false);

        cam.FocusPlayer();
    }

    public void ContinueButton()
    {
        Dialoguer.ContinueDialogue();

        
    }

}
