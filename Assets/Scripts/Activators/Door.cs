﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class Door : MonoBehaviour
{

    public enum DoorType
    {
        Door,
        SceneTransition
    } public DoorType doorType;

    public GameObject doorObject;

    public Key requiredKey;
    public bool locked;

    
    public bool loadScene;
    public int sceneID;
    public float openSpeed;

    public AudioClip sound;

    [HideInInspector]
    public AudioSource aSource;

    [HideInInspector]
    public BoxCollider boxCol;
    HingeJoint doorHinge;

    void Start()
    {
        aSource = GetComponent<AudioSource>();
        boxCol = GetComponent<BoxCollider>();
        if (doorObject.GetComponent<HingeJoint>())
        {
            doorHinge = doorObject.GetComponent<HingeJoint>();
        }
    }
    
    void FixedUpdate()
    {
        Locked();
    }

    public void Unlock()
    {
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>().keys.Contains(requiredKey))
        {
            locked = false;
        }
    }

    bool Locked()
    {
        if (locked)
        {
            boxCol.enabled = true;
        }
        else
        {
            boxCol.enabled = false;
        }

        return locked;
    }
}
