﻿using UnityEngine;
using System.Collections;

public class CombatManager : MonoBehaviour
{

    public bool combatActive = false;

    public float timeLeftInTurn = 1.0f;

    public enum TurnType
    {
        None, // When out of combat
        Player, 
        Friendly,
        Hostile
    } public TurnType turnType;

    public CombatAI currentTurn;

    private CombatAI player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<CombatAI>();
    }

    void Update()
    {
        if (combatActive)
        {
            // Is it the players turn?
            if (currentTurn.GetComponent<PlayerController>())
            {
                turnType = TurnType.Player;

                player.isMyTurn = true;
                PlayerTurn();

                return;
            }

            // Enemy turn AI
            EnemyTurn();
        }
    }

    public void StartCombat()
    {

    }

    void EndTurn()
    {
        timeLeftInTurn = 1.0f;
    }

    void EnemyTurn()
    {

    }

    void PlayerTurn()
    {
        currentTurn = player;
    }



}
