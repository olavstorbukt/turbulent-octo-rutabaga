﻿using UnityEngine;
using UnityEngine.UI;

public class InterfaceManager : MonoBehaviour
{

    public Text locationText;
    
    void Start()
    {
        GameObject.FindGameObjectWithTag("Main Interface").transform.position = Vector3.zero;
    }

    void MainInterface()
    {

    }

	public void ShowCombatInterface()
    {

    }

   public void EnterZone(string displayName)
    {
        locationText.text = displayName;
    }
}
