﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour
{

    public static int spawnPointID;

    private Transform player;
    public Vector3 startPosition;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        player.position = startPosition;
    }
	
}
