﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    // Rendering components
    public static bool motionBlur = true;


    // Volume settings
    public static float masterVolume = 1,
                        musicVolume = 1,
                        soundVolume = 1,
                        interfaceVolume = 1,
                        ambientVolume = 1,
                        speechVolume = 1;

    private PlayerController player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    public void ApplySettings()
    {

    }

    // TODO Save and Load mechanisms
    #region // Saving and Loading the game
    public void SaveGame(int saveSlot)
    {
        // Set the players position to the PlayerPrefs
        PlayerPrefs.SetFloat("posX", player.transform.position.x);
        PlayerPrefs.SetFloat("posY", player.transform.position.y);
        PlayerPrefs.SetFloat("posZ", player.transform.position.z);

        // Set what scene to load next time
        PlayerPrefs.SetInt("sceneID", PlayerController.curScene);


        // Apply settings
        PlayerPrefs.Save();
    }

    public void LoadGame(int loadSlot)
    {

    }
    #endregion
}
