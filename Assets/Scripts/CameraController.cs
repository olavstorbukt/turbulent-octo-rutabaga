﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class CameraController : MonoBehaviour
{

    public Transform player;
    public Transform focusTarget;

    public float rotSpeed = 5,
                 followSpeed = 8;
    [RangeAttribute(0, 10)]
    public float lookSensitivity = 2;

    public Vector3 offset;
    public Vector3 maxOffset; // How far away can the camera be when looking with the mouse
    
    public float zoomSpeed = 4;
    public float distance = 2; // Current zoom
    public float minDistance = 2, maxDistance = 15; // Max / max zoom

    [HideInInspector]
    public Camera cam;
    [HideInInspector]
    public MotionBlur motionBlur;

    public enum FocusMode
    {
        Player,
        TwoTargets,
        Free
    } public FocusMode focusMode;

    void Start()
    {
        cam = GetComponent<Camera>();
        motionBlur = GetComponent<MotionBlur>();
        transform.parent = null;
    }


    void LateUpdate()
    {

        if (GameSettings.motionBlur)
        {

        }

        switch (focusMode)
        {
            case FocusMode.Player:
                FocusOnTarget(player);
                
                break;
            case FocusMode.TwoTargets:
                if(Vector3.Distance(player.position, focusTarget.position) > 2f)
                {
                    FocusPlayer();
                }
                FocusBetweenPoints();
                break;
            case FocusMode.Free:
                break;
            default:
                FocusOnTarget(player);
                break;
        }
    }

    void HoverDoor(Door hoverDoor)
    {
        Color prevColor = hoverDoor.doorObject.GetComponent<Renderer>().sharedMaterial.color;

        if(Vector3.Distance(player.position, hoverDoor.transform.position) <= player.GetComponent<Actor>().interactRange)
        {
            hoverDoor.doorObject.GetComponent<Renderer>().sharedMaterial.color = Color.green;
            print("hovering " + hoverDoor.name);
        }
        else
        {
            hoverDoor.doorObject.GetComponent<Renderer>().sharedMaterial.color = prevColor;
        }
    }

    void FocusOnTarget(Transform target)
    {
       
        distance = Mathf.Clamp(distance, target.position.y + minDistance, target.position.y + maxDistance);
        //offset = new Vector3(offset.x, distance, offset.z);

        transform.position = Vector3.Lerp(transform.position, target.position + offset, followSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(75, 0, 0), 5f * Time.deltaTime)    ;
    }

    public void SetNewFocusPoints(Transform newFocusTarget)
    {
        focusTarget = newFocusTarget;
        focusMode = FocusMode.TwoTargets;
    }

    public void FocusPlayer()
    {
        
        focusMode = FocusMode.Player;

        if (GameSettings.motionBlur)
        {
            motionBlur.blurAmount = 0;
            motionBlur.enabled = false;
        }

        //speed = Input.GetButton("Run") ? 8 : 10;

        

        FocusOnTarget(player);
    }

    void LerpMotionBlurAmount(float lerpTarget, float time, float duration)
    {
        iTween.ValueTo(gameObject, iTween.Hash("name", "MotionBlurLerp", "from", motionBlur.blurAmount, "to", lerpTarget, "time", time));
    }

    public void LookTo(Vector3 pos, float speed, float duration)
    {
        focusMode = FocusMode.Free;
        iTween.MoveTo(gameObject, iTween.Hash("position", pos, "looktime", duration, "speed", speed, "oncomplete", "ExitFreeMode"));

    }

    public void JoystickOFfset(Vector3 input)
    {
        if(focusMode == FocusMode.Free)
        {
            offset.y = distance;
            
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(90, 0, 0), rotSpeed * Time.deltaTime);
            offset += input;
            transform.position = Vector3.Lerp(transform.position, player.position + offset, lookSensitivity * Time.deltaTime);

            ClampOffset();
        }
        else
        {
            return;
        }
    }

    // Internal
    // Used in the LookAt iTween function
    void ExitFreeMode()
    {
        
        focusMode = FocusMode.Player;
    }

    void FocusBetweenPoints()
    {
        offset = player.position + (focusTarget.position - player.position) / 2;
        offset.y = Vector3.Distance(player.position, focusTarget.position) * 2;

        offset.y = Mathf.Clamp(offset.y, minDistance, maxDistance);

        transform.position = Vector3.Lerp(transform.position, offset, followSpeed * Time.deltaTime);

        if (GameSettings.motionBlur)
        {
            motionBlur.blurAmount = 0.6f;
        }
       
    }

    void ClampOffset()
    {
        offset.x = Mathf.Clamp(offset.x, -maxOffset.x, maxOffset.x);
        offset.y = Mathf.Clamp(distance, minDistance, maxDistance);
        offset.z = Mathf.Clamp(offset.z, -maxOffset.z - .25f, maxOffset.z + .25f);
    }

    void OnGUI()
    {
#if UNITY_EDITOR
        GUI.Label(new Rect(0, 0, 120, 16), "TimeScale: " + Time.timeScale);
#endif
    }
}
