﻿using UnityEngine;
using System.Collections;

public class MapZone : MonoBehaviour
{

    public string displayName;

    private BoxCollider boxCol;

    public Color zoneColor;

    void Awake()
    {
        boxCol = GetComponent<BoxCollider>();
    }

    void OnTriggerStay(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<InterfaceManager>().EnterZone(displayName);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = zoneColor;
        Gizmos.DrawCube(transform.position, transform.localScale);
    }
}
