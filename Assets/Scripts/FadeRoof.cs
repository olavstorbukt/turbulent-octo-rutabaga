﻿using UnityEngine;
using System.Collections;

public class FadeRoof : MonoBehaviour
{

    public int roofMatIndex;

    public Color fadeColor = new Color(0, .2f, 0.1f, .2f);

    Color defaultColor;
    Material roofMat;
    Renderer meshRender;

    void Start()
    {
        meshRender = GetComponent<Renderer>();
        roofMat = meshRender.sharedMaterials[roofMatIndex];
        defaultColor = roofMat.color;
    }

	void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            iTween.ColorTo(gameObject, iTween.Hash("color", fadeColor, "time", .25f, "easetype", "linear"));
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            if (GetComponent<Renderer>().sharedMaterials.Length > 1)
            {
                ResetMainMaterial();
            }

        }
    }

    void OnTriggerExit(Collider col)
    {
        ResetMainMaterial();
        if (col.CompareTag("Player"))
        {
            if (GetComponent<Renderer>().sharedMaterials.Length > 1)
            {
                ResetMainMaterial();
            }
            iTween.ColorTo(gameObject, iTween.Hash("color", defaultColor, "time", .25f, "easetype", "linear"));
        }
    }

    void ResetMainMaterial()
    {
        GetComponent<Renderer>().sharedMaterials[0].color = Color.white;
    }

    
}
