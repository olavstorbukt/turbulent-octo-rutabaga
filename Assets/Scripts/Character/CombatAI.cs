﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Actor))]
public class CombatAI : MonoBehaviour
{

    public enum Team
    {
        Friendly,
        Hostile
    } public Team team;

    public enum CombatState
    {
        Idle,
        InCombat
    } public CombatState combatState;

    private Actor actor;
    private NavMeshAgent agent;

    private CombatManager combatManager;

    private Actor[] fightingAgainst;

    [HideInInspector]
    public bool isMyTurn = false;

    bool inCombat = false;

    void Start()
    {
        combatManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<CombatManager>();
        actor = GetComponent<Actor>();
        agent = actor.agent;
    }

    void Update()
    {
        if (GetComponent<PlayerController>())
        {
            return;
        }
        switch (team)
        {
            case Team.Friendly:
                FriendlyAI();
                break;
            case Team.Hostile:
                HostileAI();
                break;
        }
    }

    void FriendlyAI()
    {
        switch (combatState)
        {
            case CombatState.Idle:
                break;
            case CombatState.InCombat:

                // If it is this actors turn to attack
                if(Vector3.Distance(transform.position, actor.target.position) < actor.interactRange && isMyTurn)
                {
                    Attack();
                }
                break;
        }
    }

    void HostileAI()
    {
        switch(combatState)
        {
            case CombatState.Idle:
                break;
            case CombatState.InCombat:
                if (Vector3.Distance(transform.position, actor.target.position) < actor.interactRange && isMyTurn)
                {
                    Attack();
                }
                else
                {
                    actor.canMove = false;
                }
                
            break;
        }
    }

    void CombatChoice()
    {

    }

    public void SetNewCombatState(CombatState newState)
    {
        combatState = newState;
        // Comment
    }

    void Attack()
    {
        actor.animator.Play("attack");
    }

    public void EnterCombat(Transform combatant)
    {
        actor.SetTarget(combatant);
        combatState = CombatState.InCombat;
    }

    void SetNewTarget(Transform newTarget)
    {
        actor.target = newTarget;
    }
}
