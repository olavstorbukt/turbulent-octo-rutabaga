﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class Actor : MonoBehaviour
{

    public string displayName;
    public int health = 10;
    public int dialogueID = 0;
    
    public AudioClip[] damageSound;
    public AudioClip[] deathSound;
    public AudioClip[] greetings;
    public AudioClip[] combatTaunts;

    // System
    public float walkSpeed = 3.0f,
                 runSpeed = 4.0f,
                 interactRange = 1.0f,
                 stoppingDistance = .5f; // The actor stops if within this range to its target

    public bool canMove = true;

    // Components
    [HideInInspector]
    public NavMeshAgent agent;
    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public Vector3 walkPos;
    [HideInInspector]
    public AudioSource soundEmitter;
    [HideInInspector]
    public Inventory inventory;

    public Transform rightHoldPoint, leftHoldPoint;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        soundEmitter = transform.Find("Armature/Root/Head").GetComponent<AudioSource>();
        inventory = GetComponent<Inventory>();
    }

    void Update()
    {

        canMove = !Dialoguer.GetGlobalBoolean(0);

        if (agent.enabled)
        {
            animator.SetFloat("Speed", agent.velocity.magnitude);
        }
        


        if (target && canMove)
        {
            SetNewPosition(target.position);

            if (Vector3.Distance(transform.position, target.position) < stoppingDistance)
            {
                agent.Stop();
            }
            else
            {
                agent.Resume();
            }
        }
        
    }

    public void SetNewPosition(Vector3 pos)
    {
        agent.destination = pos;
    }

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
    }

    public void Speak(Transform lookTarget)
    {
        //iTween.LookTo(gameObject, iTween.Hash("looktarget", lookTarget, "time", 1, "axis", "y"));
        Dialoguer.SetGlobalString(1, displayName); // Assign this characters name to the dialogue
        Dialoguer.SetGlobalBoolean(0, true); // Set inDialogue bool to true
        Dialoguer.StartDialogue(dialogueID); // Start dialogue

        if (!soundEmitter.isPlaying)
        {
            int i = Random.Range(0, greetings.Length);
            soundEmitter.PlayOneShot(greetings[i]);
        }

    }

    public void TakeDamage(int amount)
    {
        health -= amount;

        if(health < 0 || health == 0)
        {
            Kill();
        }

        if (!soundEmitter.isPlaying)
        {
            int i = Random.Range(0, damageSound.Length);
            soundEmitter.PlayOneShot(damageSound[i]);
        }
    }

    void Kill()
    {
        if (soundEmitter.isPlaying)
        {
            soundEmitter.Stop();
        }

        int i = Random.Range(0, deathSound.Length);
        soundEmitter.PlayOneShot(deathSound[i]);
    }

    public void Taunt()
    {
        if (!soundEmitter.isPlaying)
        {
            int i = Random.Range(0, combatTaunts.Length);
            soundEmitter.PlayOneShot(combatTaunts[i]);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, interactRange);

        Gizmos.color = Color.green;
        Vector3 fwd = transform.TransformDirection(Vector3.forward) * interactRange;

        Gizmos.DrawRay(transform.position, fwd);
    }
	
}
