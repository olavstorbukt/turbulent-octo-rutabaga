﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Actor))]
public class PlayerController : MonoBehaviour
{
    // TODO: Player gets more tan if out in the sun much
    // 

    public static int curScene = 0;

    public float rotationSpeed = 500;
    public float pushForce = 2;

    [HideInInspector]
    public float curSpeed;

    private CameraController camControl;
    private Actor actor;
    private CharacterController controller;
    private CombatManager combatManager;
    private CombatAI combatAI;

    private Quaternion targetRotation;

    public enum ControlMode
    {
        Gamepad,
        Mouse
    } public ControlMode controlMode;

    public enum CombatState
    {
        OutOfCombat,
        InCombat
    } public CombatState combatState;

    void Start()
    {
        camControl = Camera.main.GetComponent<CameraController>();
        actor = GetComponent<Actor>();
        controller = GetComponent<CharacterController>();
        combatManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<CombatManager>();
        combatAI = GetComponent<CombatAI>();

        Dialoguer.SetGlobalString(0, actor.displayName);
    }

    void Update()
    {
    #if UNITY_EDITOR
        DebugControls();
    #endif

        if (actor.canMove)
        {
            GamepadInput();
        }
    }

    void GamepadInput()
    {
        Vector3 input = new Vector3(Input.GetAxisRaw("Left Horizontal"), 0, Input.GetAxisRaw("Left Vertical"));

        actor.agent.enabled = false;
        controller.enabled = true;

        curSpeed = Input.GetButton("Run") ? actor.runSpeed : actor.walkSpeed;


        input *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1) ? .7f : 1;
        input *= curSpeed;

        input.y += Physics.gravity.y;

        Vector3 aimInput = new Vector3(Input.GetAxisRaw("Left Horizontal"), 0, Input.GetAxisRaw("Left Vertical"));
        if (aimInput != Vector3.zero)
        {
            //camControl.JoystickOFfset(new Vector3(aimInput.x, camControl.distance, aimInput.z));
            targetRotation = Quaternion.LookRotation(aimInput);
            transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime);
        }

        Vector3 rightInput = new Vector3(Input.GetAxisRaw("Right Horizontal"), 0, Input.GetAxisRaw("Right Vertical"));
        if (rightInput != Vector3.zero)
        {
            camControl.focusMode = CameraController.FocusMode.Free;
            camControl.JoystickOFfset(rightInput);

        }else
        {
            camControl.focusMode = CameraController.FocusMode.Player;
            camControl.offset = new Vector3(0, camControl.distance, -1);
        }
        controller.Move(input * Time.deltaTime);

       

        actor.animator.SetFloat("Speed", Mathf.Sqrt(input.x * input.x + input.z * input.z));

        //pushForce = Mathf.Sqrt(input.x * input.x + input.z * input.z);

        //if (Input.GetButtonDown("Fire1"))
        //{ 
        //    actor.animator.SetTrigger("attack");
        //}

        if (Input.GetButtonDown("Activate"))
        {
            Activate();
        }

    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.collider.GetComponent<Rigidbody>())
        {
            
            Rigidbody rigid = hit.collider.attachedRigidbody;

            if (hit.moveDirection.y < -0.3F)
                return;

            if (!rigid.isKinematic)
            {
                rigid.velocity = controller.velocity * pushForce;
            }
        }
        

    }

    void Activate()
    {

        Vector3 dir = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        
            if (Physics.Raycast(transform.position, dir, out hit))
            {
                hit.distance = Mathf.Clamp(hit.distance, 0, actor.interactRange);

            // What did we try to activate?
                #region // Actor
                if (hit.collider.GetComponent<Actor>())
                {
                    Actor clickedActor = hit.collider.GetComponent<Actor>();

                    switch (clickedActor.GetComponent<CombatAI>().team)
                    {
                    #region // Friendly
                    case CombatAI.Team.Friendly:
                            if (Vector3.Distance(transform.position, clickedActor.transform.position) < clickedActor.interactRange)
                            {
                                print("Speaking with " + clickedActor.displayName);
                                camControl.SetNewFocusPoints(clickedActor.transform);
                                clickedActor.Speak(transform);
                            }
                            break;
                    #endregion
                    // Enemy Team
                    case CombatAI.Team.Hostile:
                            SelectEnemy(hit.transform);
                            break;
                    }

                    //actor.SetNewPosition(hit.collider.transform.position);


                }
                #endregion

                
                #region // Door
                if (hit.collider.GetComponent<Door>() && Vector3.Distance(transform.position, hit.collider.transform.position) <= actor.interactRange)
                {
                    hit.collider.GetComponent<Door>().Unlock();
                }
                #endregion

            }
    }

    void EnterCombat()
    {
        Time.timeScale = .25f;  
    }

    void SelectEnemy(Transform enemy)
    {
        switch (combatAI.combatState)
        {
            case CombatAI.CombatState.Idle:
                Time.timeScale = .5f;
                break;
            case CombatAI.CombatState.InCombat:
                break;
        }

        camControl.SetNewFocusPoints(enemy);
    }

    void EndTurn()
    {
        Time.timeScale = 1.0f;
    }

    void ExitTargetingMode()
    {
        Time.timeScale = 1.0f;
        camControl.FocusPlayer();
    }

    void DebugControls()
    {
        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            Time.timeScale -= .25f;
            print("New timeScale: " + Time.timeScale);
        }
        if (Input.GetKeyDown((KeyCode.Keypad9)))
        {
            Time.timeScale += .5f;
            print("New timeScale: " + Time.timeScale);
        }

        if (Input.GetKeyDown(KeyCode.Keypad7))
        {
            if (!actor.animator.GetBool("drawSword"))
            {
                actor.animator.SetTrigger("drawSword");
            }
        }

        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            camControl.LookTo(new Vector3(transform.position.x + 1, transform.position.y + 3, transform.position.z), 1, 2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            GameSettings.motionBlur = !GameSettings.motionBlur;
        }
    }
}
