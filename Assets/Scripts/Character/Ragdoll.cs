﻿using UnityEngine;
using System.Collections;

public class Ragdoll : MonoBehaviour
{
    public bool isEnabled = false;

    public Transform armature;

    [HideInInspector]
    public Rigidbody[] joints;

    private Actor actor;

    void Start()
    {
        actor = GetComponent<Actor>();

        // Assign bones
        joints = armature.GetComponentsInChildren<Rigidbody>();
    }

    void FixedUpdate()
    {
        Enabled();
    }

    bool Enabled()
    {
        foreach (Rigidbody j in joints)
        {

            j.isKinematic = !isEnabled;
            j.GetComponent<BoxCollider>().enabled = !isEnabled;

            j.useGravity = isEnabled;

            actor.agent.enabled = !isEnabled;

        }
        
        

        return isEnabled;
    }

    public void EnableRagdoll()
    {
        isEnabled = true;
    }

    public void DisableRagdoll()
    {
        isEnabled = false;
    }

}
