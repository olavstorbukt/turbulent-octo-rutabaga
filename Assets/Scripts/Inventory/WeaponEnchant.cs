﻿using UnityEngine;
using System.Collections;

public class WeaponEnchant : MonoBehaviour
{

    [Range(0, 1)]
    public float enchantHitChance = .25f;

    public int enchantPower = 1;

    public enum Enchant
    {
        None,
        Poison,
        Fire,
        Frost,
        Shock
    }
    public Enchant enchant;
}
