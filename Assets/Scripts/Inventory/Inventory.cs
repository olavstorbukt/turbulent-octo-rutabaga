﻿using UnityEngine;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{

    public MeleeWeapon meleeWeapon;
    public Bow bow;
    public Armor armor;

    public int arrows;
    public int maxArrows = 15;

    public List<Key> keys = new List<Key>();

    private Actor actor;

    [HideInInspector]
    public int armorRating = 0,
               damage = 0;

    enum WeaponMode
    {
        Melee,
        Bow
    } WeaponMode weaponMode;

    void Start()
    {

        actor = GetComponent<Actor>();

        // Equip all items
        #region 
        if (meleeWeapon)
        {
            EquipMeleeWeapon(meleeWeapon);
        }
        if (armor)
        {
            EquipArmor(armor);
        }

        #endregion
    }

    void Update()
    {
        switch (weaponMode)
        {
            case WeaponMode.Melee:
                break;
            case WeaponMode.Bow:
                break;
        }
    }

    void EquipMeleeWeapon(MeleeWeapon newMeleeWeapon)
    {
        Destroy(meleeWeapon.gameObject);
        meleeWeapon = Instantiate(newMeleeWeapon, actor.rightHoldPoint.position, Quaternion.identity) as MeleeWeapon;
        meleeWeapon.transform.parent = actor.rightHoldPoint;

        weaponMode = WeaponMode.Melee;

    }

    void EquipBow(Bow newBow)
    {

        weaponMode = WeaponMode.Bow;
    }

    void EquipArmor(Armor newArmor)
    {

    }

    void AddKey(Key newKey)
    {
        keys.Add(newKey);
        print("Added " + newKey.displayName + " to inventory");
    }

}
